#!/bin/bash
regex='cn=([a-zA-Z]*),'
file='users.txt'
rm -f $file

# clean usernames
for line in `cat ldapusers.txt`; do
    [[ $line =~ $regex ]]
    user=${BASH_REMATCH[1]}
    if [ ! -z $user ] 
    then
        echo "$user" >> $file
    fi
done

# create users

for userToCreate in `cat $file`; do
    echo "Creating user $userToCreate..."
    # useradd $userToCreate
done