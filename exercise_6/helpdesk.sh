#!/bin/bash

function pingIP
{
    echo "What IP would you like to ping?"
    read ip
    ping -c 1 $ip
}

function passwordChange
{
    echo "What username to reset password for?"
    read username
    passwd $username
}

while true
do
    clear
    select task in 'Reset password' 'Show disk usage' 'Ping a host' 'Log out'
    do
        case $REPLY in
            1 ) passwordChange ;;
            2 ) df -h ;;
            3 ) pingIP ;;
            4 ) echo "exiting..." && exit 0 ;;
            * ) echo INVALID CHOICE && exit 3 ;;
        esac
    done
done