#!/bin/bash
myDate=$(date +%d-%m-%y)

echo "The day is ${myDate:0:2}"
echo "The month is ${myDate:3:2}"
echo "The year is ${myDate:6:2}"