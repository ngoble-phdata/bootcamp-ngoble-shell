#!/bin/bash
myString='cn=lara,dc=example,dc=com'
regex='cn=([a-zA-Z]*),'
[[ $myString =~ $regex ]]
user=${BASH_REMATCH[1]}
echo "$user" | awk '{print tolower($0)}'