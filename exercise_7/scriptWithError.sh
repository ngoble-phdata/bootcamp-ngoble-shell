#!/bin/bash
#
# Define the network that is tested

resultsPath = /tmp/uplist

echo 'On what network do you want to test? (Please enter a 4 byte number)'
echo 'Press enter if you want to scan the local network'
read network
[ -z $network] && network=`ifconfig en0 | grep inet | grep -v inet6 | awk '{print $2}'`
network=${network%.*}

rm /tmp/uplist

for (( node=1; node<20; node++ )); do
    path=$network.$node
    if ping -c 1 $path; then
        echo $path >> $resultsPath
    fi
done

echo 'Press enter to display the list'
read

if [[ -d $resultsPath ]]; then
    cat "$resultsPath"
fi

exit 0